
# FOLDER TEMPLATE README
`Version : 1.0.0`  
`Creation date : 2017.09.24`  
`Modified date : 2017.09.24`  
`Author : Thomas Huang`  

## What's new
2017/09/24 : Template creation

## What is it ?
The Folder project has one purpose : To help people organize their computer folder system.  
This might not be the most efficient way for everyone to manage their folders.  
Feel free to upgrade this hierarchy and upgrade the Folder Project.  

## How to use
1. Download the folder project or clone it.  
2. Populate the empty folders with your files.  
3. Enjoy !  

## Template

    Root
    │
    ├── Archives
    │
    ├── Development
    │   └── Web
    │       ├── Backups
    │       ├── Databases
    │       ├── Projects
    │       └── Themes
    │
    ├── Documents
    │   ├── Bills
    │   ├── Certificates
    │   ├── Church
    │   ├── Job
    │   ├── Me
    │   └── Refund
    │
    ├── Library
    │   ├── Drivers
    │   ├── Fonts
    │   ├── Games
    │   ├── Images
    │   ├── Musics
    │   ├── Softwares
    │   └── Videos
    │
    └── Unsorted
